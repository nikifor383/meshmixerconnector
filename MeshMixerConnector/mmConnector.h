#pragma once

#include "StoredCommands.h"
#include "mmRemote.h"
#include <MathLib/P3D.h>

using namespace mm;
using namespace std;

typedef unsigned int Key;
class mmConnector
{
public:
	mmConnector();
	~mmConnector();

	// Read objects from a mesh file, append them to the scene, and return a list of new object IDs
	vector<int> AppendMeshFile(mmRemote& mmr, const char* pFilename);

	// Export the current selected meshes
	void ExportMeshFile(mmRemote& mmr, const char * pFilename);

	// Remove all objects from current scene
	void Clear(mmRemote& mmr);

	// Select objects by ID. Result returns objects that were actually selected
	vector<int> SelectObjects(mmRemote& mmr, const vector<int>& vObjects);

	// List objects in scene (by ID)
	vector<int> ListObjects(mmRemote& mmr);

	//do a translation to all objects
	void Translate(mmRemote &remote, const P3D &t);

	//do a rotation to all objects
	void Rotate(mmRemote &remote, const Matrix3x3 &m);

	void begin_tool(mmRemote &remote, std::string tool_name);
	void accept_tool(mmRemote &remote);
	void cancel_tool(mmRemote &remote);

	void set_toolparam(mmRemote &remote, std::string param_name, int value);
	void set_toolparam(mmRemote &remote, std::string param_name, bool value);
	void set_toolparam(mmRemote &remote, std::string param_name, float value);
	void set_toolparam(mmRemote &remote, std::string param_name, std::vector<float> value);

	void get_toolparam(mmRemote &remote, std::string param_name, int &value);
	void get_toolparam(mmRemote &remote, std::string param_name, bool &value);
	void get_toolparam(mmRemote &remote, std::string param_name, float &value);
	void get_toolparam(mmRemote &remote, std::string param_name, std::vector<float> &value);

	void tool_utility_command(mmRemote &remote, std::string command_name);
	void tool_utility_command(mmRemote &remote, std::string command_name, int nValue);
	void tool_utility_command(mmRemote &remote, std::string command_name, std::string sValue);

	std::vector<int> toolquery_new_groups(mmRemote &remote);

	mmRemote mmr;
};

