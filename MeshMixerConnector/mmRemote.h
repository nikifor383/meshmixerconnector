#pragma once
#include <WinSock2.h>
#include "StoredCommands.h"
#include <string>
#include <cstdio>
#include <iostream>

using namespace std;
using namespace mm;
using namespace rms;

class mmRemote
{
public:
	string address;
	int receive_port, send_port;
    SOCKET receive_socket, send_socket;
    SOCKADDR_IN receive_addr_in, send_addr_in;

	mmRemote();
	~mmRemote();

	void connect();
	void shutdown();
	void runCommand(mm::StoredCommands& cmd);
};

