#include "stdafx.h"
#include "mmRemote.h"


mmRemote::mmRemote()
{
	address = "127.0.0.1";
	receive_port = 0xAFDF;
	send_port = 0xAFCF;
	//debug_print = False;
}


mmRemote::~mmRemote()
{
    closesocket(receive_socket);
    closesocket(send_socket);
    WSACleanup();
}

void mmRemote::connect()
{
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;
    wVersionRequested = MAKEWORD(1, 1);
    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0) {
        cerr << "Startup Error : " << GetLastError() << endl;
        return;
    }
    if (LOBYTE(wsaData.wVersion) != 1 ||
        HIBYTE(wsaData.wVersion) != 1) {
        WSACleanup();
        cerr << "Startup Error : " << GetLastError() << endl;
        return;
    }
    // create send socket
    send_socket = socket(AF_INET, SOCK_DGRAM, 0);
    send_addr_in.sin_family = AF_INET;
    send_addr_in.sin_addr.S_un.S_addr = inet_addr(address.c_str());
    send_addr_in.sin_port = htons(send_port);
    // create receive socket
    receive_socket = socket(AF_INET, SOCK_DGRAM, 0);
    receive_addr_in.sin_family = AF_INET;
    receive_addr_in.sin_addr.S_un.S_addr = inet_addr(address.c_str());
    receive_addr_in.sin_port = htons(receive_port);
    for (int i = 0;i < 8;++i)
        receive_addr_in.sin_zero[i] = 0;
    cerr << "receive_addr : " << receive_port << ' ' << receive_addr_in.sin_port << endl;
    if (bind(receive_socket, (struct sockaddr*)&receive_addr_in, sizeof(receive_addr_in)) != 0)
    {
        cerr << "Bind Receive Socket Error : " << GetLastError() << endl;
        return;
    }
}

void mmRemote::shutdown()
{
    closesocket(receive_socket);
    closesocket(send_socket);
    WSACleanup();
}

void mmRemote::runCommand(mm::StoredCommands& cmd)
{
    const int MAXSIZE = 65536;
    char recvmsg[MAXSIZE];
    memset(recvmsg, 0, MAXSIZE);
    BinarySerializer serializer;
    cmd.Store(serializer);
    vector <unsigned char> commandBuf = serializer.buffer();
    vector <char> new_commandBuf;
    new_commandBuf.clear();
    for (int i = 0;i < commandBuf.size();++i)
        new_commandBuf.push_back((char)commandBuf[i]);
   
    // send command
    if (sendto(send_socket, &new_commandBuf[0], new_commandBuf.size(), 0, (struct sockaddr *)&send_addr_in, sizeof(struct sockaddr)) < 0) {
        cerr << "Send Error : " << GetLastError << endl;
        return;
    }
    printf("send ok...\n");
    int RET;
    // receive result
    while (1) {
        int addr_len = sizeof(struct sockaddr);

        RET = recvfrom(receive_socket, recvmsg, MAXSIZE, 0, (struct sockaddr *)&receive_addr_in, &addr_len);
        if (RET < 0) {
            continue;
        }
        break;
    }
    printf("receive ok...\n");
    vector <unsigned char> resultBuf;
    resultBuf.clear();
    for (int i = 0;i < RET;++i)
        resultBuf.push_back((unsigned char)recvmsg[i]);

    serializer.setBuffer(resultBuf);
    cmd.Restore_Results(serializer);
}