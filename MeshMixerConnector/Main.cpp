// MeshMixerConnector.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include "mmConnector.h"
#include "mmRemote.h"

int main(int argc, _TCHAR* argv[])
{
	mmRemote mmr;
	mmConnector mmc;
	vector<int> p, q, r;

    mmr.connect();
	mmc.Clear(mmr);
	p = mmc.AppendMeshFile(mmr, "C:\\Program Files\\Autodesk\\Meshmixer\\meshmixer\\models\\dog.obj");
	mmc.Translate(mmr, P3D(1, 2, 3));
	double data[9] = { 0,1,0,1,0,0,0,0,1 };
	mmc.Rotate(mmr, Matrix3x3(data));
	q = mmc.AppendMeshFile(mmr, "C:\\Program Files\\Autodesk\\Meshmixer\\meshmixer\\models\\dog.obj");
	//r.push_back(p[0]);
	//r.push_back(q[0]);
	//mmc.SelectObjects(mmr, r);
	//mmc.begin_tool(mmr, "difference");
	//mmc.accept_tool(mmr);
    mmr.shutdown();
    system("pause");
	return 0;
}

