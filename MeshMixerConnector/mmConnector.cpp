#include "stdafx.h"
#include "mmConnector.h"

mmConnector::mmConnector()
{
}


mmConnector::~mmConnector()
{
}


vector<int> mmConnector::AppendMeshFile(mmRemote& mmr, const char * pFilename) {
	StoredCommands sc;
	Key k = sc.AppendSceneCommand_AppendMeshFile(pFilename);
	mmr.runCommand(sc);
	vector<int> v;
	sc.GetSceneCommandResult_AppendMeshFileAsReference(k, v);
	return v;
}

void mmConnector::ExportMeshFile(mmRemote& mmr, const char * pFilename) {
	StoredCommands sc;
	Key k = sc.AppendSceneCommand_ExportMeshFile_CurrentSelection(pFilename);
	mmr.runCommand(sc);
}

void mmConnector::Clear(mmRemote& mmr) {
	StoredCommands sc;
	sc.AppendSceneCommand_Clear();
	mmr.runCommand(sc);
}

vector<int> mmConnector::SelectObjects(mmRemote& mmr, const vector<int> & vObjects) {
	StoredCommands sc;
	Key k = sc.AppendSceneCommand_SelectObjects(vObjects);
	mmr.runCommand(sc);
	vector<int> v;
	sc.GetSceneCommandResult_SelectObjects(k, v);
	return v;
}

vector<int> mmConnector::ListObjects(mmRemote & mmr) {
	StoredCommands sc;
	Key k = sc.AppendSceneCommand_ListObjects();
	mmr.runCommand(sc);
	vector<int> v;
	sc.GetSceneCommandResult_ListObjects(k, v);
	return v;
}

void mmConnector::Translate(mmRemote &remote, const P3D &t)
{
	StoredCommands cmd;
	begin_tool(remote, "transform");
	vector<float> f;
	f.push_back(t.at(0));f.push_back(t.at(1));f.push_back(t.at(2));
	set_toolparam(remote, "translation", f);
	accept_tool(remote);
}

void mmConnector::Rotate(mmRemote &remote, const Matrix3x3 &m)
{
	StoredCommands cmd;
	begin_tool(remote, "transform");
	vector<float> f;
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			f.push_back(m.coeff(i, j));
	set_toolparam(remote, "rotation", f);
	accept_tool(remote);
}

void mmConnector::begin_tool(mmRemote &remote, std::string tool_name)
{
	StoredCommands cmd;
	cmd.AppendBeginToolCommand(tool_name);
	remote.runCommand(cmd);
}

void mmConnector::accept_tool(mmRemote &remote)
{
	StoredCommands cmd;
	cmd.AppendCompleteToolCommand("accept");
	remote.runCommand(cmd);
}
void mmConnector::cancel_tool(mmRemote &remote)
{
	StoredCommands cmd;
	cmd.AppendCompleteToolCommand("cancel");
	remote.runCommand(cmd);
}

void mmConnector::set_toolparam(mmRemote &remote, std::string param_name, int value)
{
	StoredCommands cmd;
	cmd.AppendToolParameterCommand(param_name, value);
	remote.runCommand(cmd);
}

void mmConnector::set_toolparam(mmRemote &remote, std::string param_name, bool value)
{
	StoredCommands cmd;
	cmd.AppendToolParameterCommand(param_name, value);
	remote.runCommand(cmd);
}

void mmConnector::set_toolparam(mmRemote &remote, std::string param_name, float value)
{
	StoredCommands cmd;
	cmd.AppendToolParameterCommand(param_name, value);
	remote.runCommand(cmd);
}

void mmConnector::set_toolparam(mmRemote &remote, std::string param_name, std::vector<float> value)
{
	StoredCommands cmd;
	if (value.size() == 3)
		cmd.AppendToolParameterCommand(param_name, value[0], value[1], value[2]);
	else
		if (value.size() == 9)
		cmd.AppendToolParameterCommand(param_name, value[0], value[1], value[2], value[3], value[4], value[5], value[6], value[7], value[8]);
		else //wrong
			;
	remote.runCommand(cmd);
}

void mmConnector::get_toolparam(mmRemote &remote, std::string param_name, int &value)
{
	StoredCommands cmd;
	Key key = cmd.AppendGetToolParameterCommand(param_name);
	remote.runCommand(cmd);
	any_result ret;
	bool bfound = cmd.GetToolParameterCommandResult(key, ret);
	if (bfound)
		value = ret.i;
	else //wrong
		;
}

void mmConnector::get_toolparam(mmRemote &remote, std::string param_name, bool &value)
{
	StoredCommands cmd;
	Key key = cmd.AppendGetToolParameterCommand(param_name);
	remote.runCommand(cmd);
	any_result ret;
	bool bfound = cmd.GetToolParameterCommandResult(key, ret);
	if (bfound)
		value = ret.b;
	else //wrong
		;
}

void mmConnector::get_toolparam(mmRemote &remote, std::string param_name, float &value)
{
	StoredCommands cmd;
	Key key = cmd.AppendGetToolParameterCommand(param_name);
	remote.runCommand(cmd);
	any_result ret;
	bool bfound = cmd.GetToolParameterCommandResult(key, ret);
	if (bfound)
		value = ret.f;
	else //wrong
		;
}

void mmConnector::get_toolparam(mmRemote &remote, std::string param_name, vector<float> &value)
{
	StoredCommands cmd;
	Key key = cmd.AppendGetToolParameterCommand(param_name);
	remote.runCommand(cmd);
	any_result ret;
	bool bfound = cmd.GetToolParameterCommandResult(key, ret);
	if (bfound)
	{
		value.clear();
		if (ret.type == 3)
		{
			value.push_back(ret.x);
			value.push_back(ret.y);
			value.push_back(ret.z);
		}
		else
			if (ret.type == 4)
			for (int i = 0; i < 9; ++i)
			value.push_back(ret.m[i]);
			else //wrong
				;
	}
	else //wrong
		;
}

void mmConnector::tool_utility_command(mmRemote &remote, std::string command_name)
{
	StoredCommands cmd;
	cmd.AppendToolUtilityCommand(command_name);
	remote.runCommand(cmd);
}

void mmConnector::tool_utility_command(mmRemote &remote, std::string command_name, int nValue)
{
	StoredCommands cmd;
	cmd.AppendToolUtilityCommand(command_name, nValue);
	remote.runCommand(cmd);
}

void mmConnector::tool_utility_command(mmRemote &remote, std::string command_name, std::string sValue)
{
	StoredCommands cmd;
	cmd.AppendToolUtilityCommand(command_name, sValue);
	remote.runCommand(cmd);
}

std::vector<int> mmConnector::toolquery_new_groups(mmRemote &remote)
{
	StoredCommands cmd;
	Key key = cmd.AppendToolQuery_NewGroups();
	remote.runCommand(cmd);
	std::vector<int> new_groups;
	bool bOK = cmd.GetToolQueryResult_NewGroups(key, new_groups);
	return new_groups;
}
