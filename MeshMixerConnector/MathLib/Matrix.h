#pragma once

#include <eigen3.2.5/Eigen/dense>
#include <eigen3.2.5/Eigen/sparse>

typedef Eigen::Vector3d Vector3d;
typedef Eigen::VectorXd dVector;
typedef Eigen::MatrixXd Matrix;
typedef Eigen::Matrix2d Matrix2x2;
typedef Eigen::Matrix3d Matrix3x3;
typedef Eigen::Matrix4d Matrix4x4;
typedef Eigen::SparseMatrix<double> SparseMatrix;
typedef Eigen::Triplet<double> SMTriplet;

inline void resize(SparseMatrix& sm, int rows, int cols) {
	if (sm.rows() != rows || sm.cols() != cols)
		sm.resize(rows, cols);
	else
		sm.setZero();
}

inline void resize(dVector& v, int n) {
	if (v.size() != n)
		v.resize(n);
	else
		v.setZero();
}

inline void resize(Matrix& m, int rows, int cols) {
	if (m.rows() != rows || m.cols() != cols)
		m.resize(rows, cols);
	else
		m.setZero();
}

//TODO: want to write only upper (or lower?) values for symmetric matrices
template<class MATType>
void writeSparseMatrixDenseBlock(SparseMatrix& hes, int startX, int startY, const MATType& block, bool writeOnlyUpperDiagonalValues) {
	for (int i = 0; i < block.rows(); i++)
		for (int j = 0; j < block.cols(); j++)
			hes.coeffRef(startX + i, startY + j) = block(i, j);
}

template <class MATType>
void print(char* fName, const MATType& mat) {
	FILE* fp; fp = fopen(fName, "w");

	for (int i = 0;i<mat.rows();i++) {
		for (int j = 0;j<mat.cols();j++) {
			fprintf(fp, "%f\t", mat.coeff(i, j));
		}
		fprintf(fp, "\n");
	}
	fclose(fp);
}

