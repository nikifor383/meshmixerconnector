#pragma once

#include <MathLib/SparseMatrix.h>
#include <Utils/Utils.h>

/*
	A multi-dimensional function that expresses linear equality and inequality constraints that might be applied to an objective function.
*/
class FunctionConstraints{
protected:
	SparseMatrix A, C;
	dVector b, c, f, minVals, maxVals, eqConstraintVals, ineqConstraintVals;
public:
	FunctionConstraints();
	virtual ~FunctionConstraints();

	/*
		assume the constraints are either linear, or they should be linearized at p...
	*/

	//equality constraints of the form Ap = b
	virtual SparseMatrix* getEqualityConstraintsJacobian(const dVector& p) {
		estimateEqualityConstraintsJacobianAt(A, p);
		return &A;
	}

	virtual dVector* getEqualityConstraintsTargetValues() {return &b;}
	//get the actual value of the equality constraints...
	virtual dVector* getEqualityConstraintValues(const dVector& p){return &eqConstraintVals;}

	//inequality constraints of the form d <= Cp <= f
	virtual SparseMatrix* getInequalityConstraintsJacobian(const dVector& p) {
		estimateInequalityConstraintsJacobianAt(C, p);
		return &C;
	}

	virtual dVector* getInequalityConstraintsMinValues() {return &c;}
	virtual dVector* getInequalityConstraintsMaxValues() {return &f;}
	//get the actual value of the equality constraints...
	virtual dVector* getInequalityConstraintValues(const dVector& p) {return &ineqConstraintVals;}

	//and bound constraints for the parameters min <= p <= max
	virtual dVector* getBoundConstraintsMinValues() {return &minVals;}
	virtual dVector* getBoundConstraintsMaxValues() {return &maxVals;}

	void estimateEqualityConstraintsJacobianAt(SparseMatrix& J, const dVector& p);
	void estimateInequalityConstraintsJacobianAt(SparseMatrix& J, const dVector& p);

	void testJacobiansWithFD(const dVector& p);
};

