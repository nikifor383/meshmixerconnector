#include "NewtonFunctionMinimizer.h"

//TODO: try out different linear solvers, with prefactorization and without. Have a way of specifying which one this newton solver should be using...
//TODO: add suitesparse to the project as well for modified cholesky - get eigen to use it.
//TODO: ask gaurav which qp solver he was using - probably ooqp but via the eigen sparse matrix class
//TODO: get a version of the objective functions (and everything else) that works with dense matrices as well...

NewtonFunctionMinimizer::NewtonFunctionMinimizer(int p_maxNewtonIterations, double p_newtonSolveResidual, int p_maxLineSearchIterations, bool p_printOutput, bool p_svdAnalysis){
	maxNewtonIterations = p_maxNewtonIterations;
	maxLineSearchIterations = p_maxLineSearchIterations;
	printOutput = p_printOutput;
	svdAnalysis = p_svdAnalysis;
	newtonSolveResidual = p_newtonSolveResidual;

//	printOutput = true;
}

NewtonFunctionMinimizer::~NewtonFunctionMinimizer(){
}

bool NewtonFunctionMinimizer::minimize(ObjectiveFunction *function, dVector &p, double& functionValue){
	if(printOutput){
		Logger::logPrint("Starting Newton's method...\n");
	}

	//number of parameters...
	int N = (int) p.size();
	dVector pi(N), dp(N), gradient(N);
	pi = p;

	function->setCurrentBestSolution(pi);

	bool optimizationConverged = false;
	for(iterationsUsed=0; iterationsUsed < maxNewtonIterations; iterationsUsed++) {
		gradient = *function->getGradientAt(pi);
		/////////////
		//function->testGradientWithFD(pi);
		//function->testHessianWithFD(pi);
		/////////////

		if (printOutput)
			Logger::logPrint("Starting iteration %d. Initial function value: %10.10lf. Gradient norm: %lf\n", iterationsUsed, function->computeValue(pi), gradient.norm());

		double normMag = gradient.norm();
//		if (printOutput) Logger::logPrint3("gradient norm: %10.10lf\n", normMag);
		if (normMag < newtonSolveResidual){
			optimizationConverged = true;
			break;
		}

		if (printOutput) timerN.restart();
		computeSearchDirection(function->getHessianAt(pi), gradient, dp);
		//dp = function->getCurrentGradient();

		double alpha = doLineSearch(function, pi, dp, maxLineSearchIterations);
		pi += dp * (-alpha);
		function->setCurrentBestSolution(pi);
	}

	functionValue = function->computeValue(pi);

	if(printOutput) {
		gradient = *function->getGradientAt(pi);
		Logger::logPrint("Done optimization step. Final function value: %10.10lf\n", function->computeValue(pi));
		
		if (optimizationConverged){
			Logger::logPrint("Converged in %d iterations!!! Gradient norm: %lf. FunctionValue: %10.10lf\n", iterationsUsed, gradient.norm(), function->computeValue(pi));
		}else{
			Logger::logPrint("Did NOT converge!!! Gradient norm: %lf. FunctionValue: %lf\n", gradient.norm(), function->computeValue(pi));
		}
	}

	if(svdAnalysis){
		//TODO...
//		SVDFactorization svd;
//		SparseMatrix U,S,V;
//		svd.computeSVD(*function->getHessianAt(pi),U,S,V);
//		for(int i=0;i<min(S.getNumCols(),S.getNumRows());i++)
//			Logger::printStatic("%d th singluer value of hessian : %e \n",i+1,S.getElementAt(i,i));
	}

	//p now holds the parameter values at the start of the iteration...
	p = pi;
	//and done!
	return optimizationConverged;
}

void NewtonFunctionMinimizer::computeSearchDirection(SparseMatrix* hessian, const dVector& gradient, dVector& dp){
	if (printOutput){
		Logger::logPrint("Time to compute gradient, hessian, etc: %lf\n", timerN.timeEllapsed());
		timerN.restart();
	}
//	hessian->printMatrix("out/hes.m");
//	gradient.printVector("out/grad.m");

	//dp = Hes^-1 * grad
	Eigen::SimplicialLDLT<SparseMatrix, Eigen::Upper> solver;
	solver.compute(*hessian);
	dp = solver.solve(gradient);

	if (printOutput){
		Logger::logPrint("Time to compute search direction: %lf\n", timerN.timeEllapsed());
		timerN.restart();
	}

	double dot = 0;
	for(int i=0;i<dp.size();i++)
		dot+=dp[i]*gradient[i];
	
	if(dot<0){
		dp=gradient;
	}else{
		int checkLinearSystemSolve = 1;
		if (checkLinearSystemSolve){
			dVector pn = dp;
			// check how well we solved the linear system
			dVector rhs((int)gradient.size());
			rhs = *hessian * dp;
			rhs -= gradient;
			double residual = rhs.norm();
			if (printOutput && residual > 1e-10)
				Logger::logPrint("linear system residual: %10.10lf\n", residual);
		}
	}
}


double NewtonFunctionMinimizer::doLineSearch(ObjectiveFunction *function, const dVector& pi, const dVector& dp, int maxIterations){
	// line search now...
	double alpha = 1.0;
	dVector pc(pi);
	double initialValue = function->computeValue(pc);

	for(int j = 0; j < maxIterations; j++) {
		// pi = pi + dp (note, the "-" is because the system we're solving is: grad = -hes * dp
		pc = pi - dp * alpha;

		// now check the new function value at this point...
		double newLineSearchValue = function->computeValue(pc);

		if (printOutput)
			Logger::logPrint("\t--> LINE SEARCH iteration %02d: alpha is %10.10lf, function value is: %10.10lf\n", j, alpha, newLineSearchValue);

		if(newLineSearchValue >= initialValue && j < maxIterations-1) {
			// restore and try again...
			alpha /= 2.0;
		} else {
			// found a better solution!
			return alpha;
		}
	}

	// couldn't find a good value. Return what we now have and hope for the best...
	return alpha;
}


