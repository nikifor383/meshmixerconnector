#pragma once

#include "LinearLeastSquaresSolver.h"
#include "ConstrainedObjectiveFunction.h"
#include "QPSolverMosek.h"
#include "ObjectiveFunction.h"

/**
	Use the Sequential Quadratic Programming method to optimize a function, subject to constraints.
	
	Task: Find p that minimize f(p), such that Ap = b and d <= Cp <= f
	This means df/dp(p) = 0. SQP works similar as Newton iteration, i.e. approximates the environment of f as
	f(p + dp) ~ f(p) + df/dp(p)*dp + 1/2 dp' * d/dp(df/dp)(p) * dp
	Which is minimized when df/dp is closest to zero within the constraints.

	SQP hence solves a sequence of QP problems of the form
		min d/dp(df/dp)*dp + df/dp, s.t. A(p + dp) = b and C(p + dp) <= f
	which gives us the stepping direction dp within the constraint manifold. Iterating the above will
	hopefully yield p that minimizes f.

	A warning: Convergence can be improved in some cases by doing line search in the direction of dp.
	However, this can give us intermediate points outside the constraint manifold, which is bad.
	If you're unsure whether your constraints are non-convex, set 'maxLineSearchIterations' to 0.
*/
class SQPFunctionMinimizer{
public:
	bool printOutput;
	int maxLineSearchIterations;
	int maxIterations;

	SQPFunctionMinimizer(int maxIterations = 2000, double solveResidual=0.0001, double regularizer = 0.0, int maxLineSearchIterations = 0, double solveFunctionValue = DBL_MIN, bool printOutput = false, bool checkConstraints = false);
	~SQPFunctionMinimizer();

	/**
		min f(p) subject to the constraints...
	*/
	bool minimize(ConstrainedObjectiveFunction *function, dVector &p, double &functionValue);


private:
	void computeSearchDirection(SparseMatrix* hessian, dVector* gradient, const dVector &p, dVector &dp, SparseMatrix *A, dVector *b, dVector *f, SparseMatrix *C, dVector *d, dVector *minBounds, dVector *maxBounds);
	double doLineSearch(ObjectiveFunction *function, const dVector &p, const dVector &dp, int maxSteps);

private:
	double solveResidual;
	double solveFunctionValue;
	double regularizer;


	bool checkConstraints;


	LinearLeastSquaresSolver lsSolver;

#if USE_MOSEK_QP_SOLVER
	QPSolverMosek qpSolver;
#else
	QPSolver qpSolver;
#endif
};
