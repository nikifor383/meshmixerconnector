#include "FunctionConstraints.h"

FunctionConstraints::FunctionConstraints(){
}

FunctionConstraints::~FunctionConstraints(){
}

void FunctionConstraints::estimateEqualityConstraintsJacobianAt(SparseMatrix& J, const dVector& params){
	dVector pSet = params;

	int nConstraints = (int)getEqualityConstraintValues(pSet)->size();

	double dp = 10e-6;
	int p = (int)pSet.size();
	J.resize(nConstraints, p);
	J.zero();

	dVector C_P(nConstraints,0), C_M(nConstraints,0), J_i_col(nConstraints,0);
	//this is a very slow method that evaluates the jacobian of the objective function through FD...
	for (int i=0;i<p;i++){
		double tmpVal = pSet[i];
		pSet[i] = tmpVal + dp;
		copy(*getEqualityConstraintValues(pSet), C_P);

		pSet[i] = tmpVal - dp;
		copy(*getEqualityConstraintValues(pSet), C_M);

		//now reset the ith param value
		pSet[i] = tmpVal;
		add(C_P, 1.0/(2*dp), C_M, -1.0/(2*dp), J_i_col);

		//each vector is a column vector of the hessian, so copy it in place...
		for (int j=0;j<nConstraints;j++)
			if (!IS_ZERO(J_i_col[j]))
				J.setElementAt(j, i, J_i_col[j]);
	}
}

void FunctionConstraints::estimateInequalityConstraintsJacobianAt(SparseMatrix& J, const dVector& params){
	dVector pSet = params;

	int nConstraints = (int)getInequalityConstraintValues(pSet)->size();

	double dp = 10e-6;
	int p = (int)pSet.size();
	J.resize(nConstraints, p);
	J.zero();

	dVector C_P(nConstraints,0), C_M(nConstraints,0), J_i_col(nConstraints,0);
	//this is a very slow method that evaluates the jacobian of the objective function through FD...
	for (int i=0;i<p;i++){
		double tmpVal = pSet[i];
		pSet[i] = tmpVal + dp;
		copy(*getInequalityConstraintValues(pSet), C_P);

		pSet[i] = tmpVal - dp;
		copy(*getInequalityConstraintValues(pSet), C_M);

		//now reset the ith param value
		pSet[i] = tmpVal;
		add(C_P, 1.0/(2*dp), C_M, -1.0/(2*dp), J_i_col);

		//each vector is a column vector of the hessian, so copy it in place...
		for (int j=0;j<nConstraints;j++)
			if (!IS_ZERO(J_i_col[j]))
				J.setElementAt(j, i, J_i_col[j]);
	}
}

void FunctionConstraints::testJacobiansWithFD(const dVector& p){
	SparseMatrix FDJacobian;
	estimateEqualityConstraintsJacobianAt(FDJacobian, p);
	SparseMatrix* analyticJacobian = getEqualityConstraintsJacobian(p);

	logPrint("Objective Function: testing equality constraints jacobian...\n");
	if (analyticJacobian != NULL){
		for (int i=0;i<FDJacobian.getNumRows();i++){
			for (uint j=0;j<p.size();j++){
				double err = FDJacobian.getElementAt(i,j) - analyticJacobian->getElementAt(i,j);
				if (fabs(err) > 0.001)
					logPrint("Mismatch element %d,%d: Analytic val: %lf, FD val: %lf. Error: %lf\n", i, j, analyticJacobian->getElementAt(i,j), FDJacobian.getElementAt(i,j), err);
			}
		}
	}

	estimateInequalityConstraintsJacobianAt(FDJacobian, p);
	analyticJacobian = getInequalityConstraintsJacobian(p);

	if (analyticJacobian != NULL){
		logPrint("Objective Function: testing inequality constraints jacobian...\n");
		for (int i=0;i<FDJacobian.getNumRows();i++){
			for (uint j=0;j<p.size();j++){
				double err = FDJacobian.getElementAt(i,j) - analyticJacobian->getElementAt(i,j);
				if (fabs(err) > 0.001)
					logPrint("Mismatch element %d,%d: Analytic val: %lf, FD val: %lf. Error: %lf\n", i, j, analyticJacobian->getElementAt(i,j), FDJacobian.getElementAt(i,j), err);
			}
		}
	}
}


