#pragma once

#include "MathLib/Matrix.h"
#include <Utils/Utils.h>

/**
	TODO: 
	- make sure the solvers/minimizers don't compute objective values/gradients unecessarily...
	- create an abstract interface for the various minimizers, including the constraint solvers...
	- revisit sparse vs dense matrices, abstract solvers for all of these... add MA27 (MKL?) solvers for the sparse matrices (in addition to CholMod), and some dense solvers from lapack (svd-based, maybe also LU decomposition-based, or something like that)
	- revisit interface for QP solvers (ooqp, mosek, quadprog)...
	- create a common interface for Matrix, that will cover both SparseMatrix and DenseMatrix. Each one with its types of solvers, but then an objective function can return whichever type of matrix it wants to. And also, each matrix should return a prefered type of solver that knows how to deal with it...
	- clean up dVector, and make it work (multiplication, system solve) with both sparse and dense matrices
*/
class ObjectiveFunction{
public:
	ObjectiveFunction();
	virtual ~ObjectiveFunction();

	//this should always return the current value of the objective function
	virtual double computeValue(const dVector& p) = 0;
	virtual SparseMatrix* getHessianAt(const dVector& p) {return NULL;}
	virtual dVector* getGradientAt(const dVector& p){return NULL;}
	//this method gets called whenever a new best solution to the objective function is found
	virtual void setCurrentBestSolution(const dVector& p) { bestSolutionYet = p; }

	void estimateGradientAt(dVector& gradient, const dVector& p);
	void estimateHessianAt(SparseMatrix& hessian, const dVector& p);

	void testGradientWithFD(const dVector& p);
	void testHessianWithFD(const dVector& p);

private:
	SparseMatrix FDHessian;

public:
	dVector bestSolutionYet;
	std::vector<SMTriplet> triplets;
};

class SubObjective{
public:
	char* description;
	double weight;
	ObjectiveFunction* objective;

	SubObjective(char* description, double weight, ObjectiveFunction* objective){
		this->description = description;
		this->weight = weight;
		this->objective = objective;
	}

	SubObjective(){
		this->description = 0;
		this->weight = 0;
		this->objective = 0;
	}
};