#pragma once

#include "ObjectiveFunction.h"

class GradientDescentFunctionMinimizer{
public:
	GradientDescentFunctionMinimizer(int p_maxIterations = 100, double p_solveResidual=0.0001, int p_maxLineSearchIterations = 15, bool p_printOutput = false);
	virtual ~GradientDescentFunctionMinimizer();

	/**
		use gradient descent to optimize a function. Since the gradient of a function gives the direction
		of steepest descent, all one needs to do is go in that direction, but we'll use a line search to know how far to go...
	*/
	virtual bool minimize(ObjectiveFunction *function, dVector &p, double & functionValue);
	virtual double doLineSearch(ObjectiveFunction *function, const dVector& pi, const dVector& dp, int maxIterations, double initialFunctionValue);

	bool printOutput;
protected:
	double solveResidual;
	int maxIterations;
	int maxLineSearchIterations;

};

