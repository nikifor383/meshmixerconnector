#include "ConstrainedObjectiveFunction.h"

ConstrainedObjectiveFunction::ConstrainedObjectiveFunction(ObjectiveFunction* obj, FunctionConstraints* C){
	this->obj = obj;
	this->C = C;
}

ConstrainedObjectiveFunction::~ConstrainedObjectiveFunction(){
}

void ConstrainedObjectiveFunction::setCurrentBestSolution(const dVector& p){
	this->obj->setCurrentBestSolution(p);
}

