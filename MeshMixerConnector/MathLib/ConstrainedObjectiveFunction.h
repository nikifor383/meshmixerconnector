#pragma once

#include <MathLib/SparseMatrix.h>
#include <Utils/Utils.h>

#include "ObjectiveFunction.h"
#include "FunctionConstraints.h"

/*
	A multi-dimensional function that expresses linear equality and inequality constraints that might be applied to an objective function.
*/
class ConstrainedObjectiveFunction{
private:
	ObjectiveFunction* obj;
	FunctionConstraints* C;
public:
	ConstrainedObjectiveFunction(ObjectiveFunction* obj, FunctionConstraints* C);
	~ConstrainedObjectiveFunction();

	ObjectiveFunction* getObjectiveFunction(){return obj;}
	FunctionConstraints* getFunctionConstraints(){return C;}

	void setCurrentBestSolution(const dVector& p);
};

