#pragma once

#include "ObjectiveFunction.h"
#include <Utils/Timer.h>


class NewtonFunctionMinimizer{
public:
	NewtonFunctionMinimizer(int p_maxNewtonIterations = 2000, double p_newtonSolveResidual=0.0001, int p_maxLineSearchIterations = 15, bool p_printOutput = false, bool p_svdAnalysis = false);
	virtual ~NewtonFunctionMinimizer();

	/**
		use Newton's method to optimize a function. p will store the final value that minimizes the function, and its initial value
		is used to start the optimization method.

		Task: find p that minimize f(p). This means that df/dp(p) = 0.
		df/dp(p+dp) ~ df/dp(p) + d/dp(df/dp) * dp = 0 ==> -df/dp(p) = d/dp(df/dp) * dp
		Iterating the above, will hopefully get p that minimizes f.
	*/
	virtual bool minimize(ObjectiveFunction *function, dVector &p, double & functionValue);
	virtual void computeSearchDirection(SparseMatrix* hessian, const dVector& gradient, dVector& dp);
	virtual double doLineSearch(ObjectiveFunction *function, const dVector& pi, const dVector& dp, int maxIterations);

	bool printOutput;

	int getIterationsUsed() { return iterationsUsed; }
protected:
	double newtonSolveResidual;
	int maxNewtonIterations;
	int maxLineSearchIterations;
	int iterationsUsed;

	bool svdAnalysis;

	Timer timerN;
};

