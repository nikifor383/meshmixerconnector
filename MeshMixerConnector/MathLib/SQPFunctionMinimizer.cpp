#include "SQPFunctionMinimizer.h"
#include "ObjectiveFunction.h"
#include <fstream>
#include <Utils/Timer.h>

Timer timer;

SQPFunctionMinimizer::SQPFunctionMinimizer(int maxIterations, double solveResidual, double regularizer, int maxLineSearchIterations, double solveFunctionValue, bool printOutput, bool checkConstraints)
	:	maxIterations(maxIterations),
		solveResidual(solveResidual),
		regularizer(regularizer),
		maxLineSearchIterations(maxLineSearchIterations),
		solveFunctionValue(solveFunctionValue),
		printOutput(printOutput),
		checkConstraints(checkConstraints)
{
}

SQPFunctionMinimizer::~SQPFunctionMinimizer(){
}

/**
	min f(p) subject to the constraints...
*/
bool SQPFunctionMinimizer::minimize(ConstrainedObjectiveFunction *function, dVector &p, double &functionValue){
	if (printOutput)
		logPrint("Starting SQP...\n");

	int N = (int)p.size();
	dVector pi(N,0), dp(N,0), gradient(N,0);
	copy(p, pi);

	function->setCurrentBestSolution(pi);

	// Iterate - like Newton
	bool optimizationConverged = false;
	int i;
	for (i=0; i<maxIterations; i++)	{
		gradient = *function->getObjectiveFunction()->getGradientAt(pi);
		// Find the direction to step at
		if (printOutput) timer.restart();
		computeSearchDirection(function->getObjectiveFunction()->getHessianAt(pi), &gradient, pi, dp, function->getFunctionConstraints()->getEqualityConstraintsJacobian(pi), function->getFunctionConstraints()->getEqualityConstraintsTargetValues(), function->getFunctionConstraints()->getInequalityConstraintsMinValues(), function->getFunctionConstraints()->getInequalityConstraintsJacobian(pi), function->getFunctionConstraints()->getInequalityConstraintsMaxValues(), function->getFunctionConstraints()->getBoundConstraintsMinValues(), function->getFunctionConstraints()->getBoundConstraintsMaxValues());

		if (printOutput)
			logPrint("Starting iteration %d. Initial function value: %10.10lf. Search direction norm: %lf. Gradient norm: %lf\n", i, function->getObjectiveFunction()->computeValue(pi), getNorm(dp), getNorm(gradient));

		if (getNorm(dp) < solveResidual || function->getObjectiveFunction()->computeValue(pi) < solveFunctionValue)	{
			// We're done
			optimizationConverged = true;
			break;
		}

		// Do a line search
		double alpha = doLineSearch(function->getObjectiveFunction(), pi, dp, maxLineSearchIterations);
		if (alpha <= 0)
		{
			logPrint("\tCancelling iteration: No better function value found\n");
			optimizationConverged = true;
			break;
		}

		// p(i+1) = p(i) + alpha*dp
		add(pi, 1, dp, alpha, pi);
		function->setCurrentBestSolution(pi);
	}

	functionValue = function->getObjectiveFunction()->computeValue(pi);

	if (printOutput) {
		logPrint("===== Done SQP optimization =====\n");
		if (optimizationConverged)
			logPrint("   Converged in %d iterations!\n", i);
		else
			logPrint("   Did NOT converge!\n");

		logPrint("   Final function value: %10.10lf\n", functionValue);
		logPrint("   Final Gradient norm: %10.10lf\n", getNorm(gradient));
		logPrint("   Final search direction norm: %10.10lf\n", getNorm(dp));

		int ineqConstraintsSize = function->getFunctionConstraints()->getInequalityConstraintValues(pi)->size();
		int eqConstraintsSize = function->getFunctionConstraints()->getEqualityConstraintValues(pi)->size();

		if (eqConstraintsSize > 0){
			dVector de = *function->getFunctionConstraints()->getEqualityConstraintValues(pi);
			dVector dtargets = *function->getFunctionConstraints()->getEqualityConstraintsTargetValues();
			double maxError = 0;
			for (uint i=0; i<de.size(); i++)
				maxError = MAX(maxError, fabs(de[i]-dtargets[i]));
			if (maxError > 0.00001)
				logPrint("-----> Max equality constraint error: %10.10lf\n", maxError);
		}

		if (ineqConstraintsSize > 0){
			dVector de = *function->getFunctionConstraints()->getInequalityConstraintValues(pi);
			double maxError = 0;
			for (uint i=0; i<de.size(); i++){
				maxError = MAX(maxError, -de[i]+function->getFunctionConstraints()->getInequalityConstraintsMinValues()->at(i));
				maxError = MAX(maxError, de[i]-function->getFunctionConstraints()->getInequalityConstraintsMaxValues()->at(i));
			}
			if (maxError > 0.00001)
				logPrint("------> Max inequality constraint error: %10.10lf\n", maxError);
		}

		dVector minVals = *function->getFunctionConstraints()->getBoundConstraintsMinValues();
		dVector maxVals = *function->getFunctionConstraints()->getBoundConstraintsMaxValues();

		if (minVals.size() == maxVals.size() && minVals.size() == p.size()){
			for (uint i=0;i<p.size();i++)
				if (minVals[i] != maxVals[i] && (minVals[i]>pi[i] || pi[i]>maxVals[i]))
					logPrint("-------> Bound %d: %lf < %lf < %lf\n", i, minVals[i], pi[i], maxVals[i]);
		}

		logPrint("\n");
	}

	copy(pi, p);
	return optimizationConverged;

}

void SQPFunctionMinimizer::computeSearchDirection(SparseMatrix* hessian, dVector* gradient, const dVector &p, dVector &dp, SparseMatrix *A, dVector *b, dVector *d, SparseMatrix *C, dVector *f, dVector *minBounds, dVector *maxBounds){
	if (printOutput){
		logPrint("Time to compute gradient, hessian, etc: %lf\n", timer.timeEllapsed());
		timer.restart();
	}

	// We solve the system of equations
	//  H*dp + grad == 0
	// in a least squares sense while maintaining the constraints A*p = b and d <= C*p <= f.
	// This means we minimize
	//  |H*dp + grad|^2, s.t. A*(p + dp) = b and d <= C*(p + dp) <= f
	dVector fMinusCp = *f, dMinusCp = *d;
	if (C->getNumCols() == p.size()){
		C->addMultVector(p, fMinusCp, -1);
		C->addMultVector(p, dMinusCp, -1);
	}

	dVector bMinusAp = *b;
	if (A->getNumCols() == p.size())
		A->addMultVector(p, bMinusAp, -1);

	dVector minMinusdp = *minBounds, maxMinusdp = *maxBounds;
	if (minMinusdp.size() == p.size() && maxMinusdp.size() == p.size()){
		minMinusdp.add(p, -1);
		maxMinusdp.add(p, -1);
	}else{
		minMinusdp.resize(p.size(), 0);
		maxMinusdp.resize(p.size(), 0);
	}

	bool success = qpSolver.solve(*hessian, *gradient, *A, bMinusAp, *C, dMinusCp, fMinusCp, minMinusdp, maxMinusdp, dp);

//	hessian->printMatrix("out/H.m");
//	gradient->printVector("out/g.m");
//	dp.printVector("out/dp.m");
//	A->printMatrix("out/A.m");


	if (printOutput){
		logPrint("Time to compute search direction: %lf\n", timer.timeEllapsed());
		timer.restart();
	}

	if (success && printOutput)
		logPrint("	==> Search direction: dot product between gradient and search direction: %lf\n", -dotprod(*gradient, dp));

	if (!success){
		assert("Failed QP solve in SQP - printing system and returning zero stepping direction" && false);
		dp.zero();
	}
	
	// Make sure the solution always stays within the constraints.
	if (checkConstraints){
		dVector pnext;
		pnext.setToSum(p, 1, dp, 1);

		// p must satisfy A*p = b after the solve...
		if (A->getNumCols() == p.size()){
			dVector ap, apNext;
			A->multVector(p, ap);
			A->multVector(pnext, apNext);
			for (uint i=0; i<ap.size(); i++){
				if (abs(ap[i] - b->at(i)) >= 0.0000001 || abs(apNext[i] - b->at(i)) >= 0.0000001)
					logPrint("++++++++ SQP: equality constraint %d: p: %.10lf, p+dp: %.10lf, constraint value: %lf\n", i, ap[i], apNext[i], b->at(i));
			}
		}

		// p must satisfy d <= C*p <= f
		if (C->getNumCols() == p.size()){
			dVector cp, cpNext;
			C->multVector(p, cp);
			C->multVector(pnext, cpNext);

			for (uint i=0; i<cp.size(); i++){
//				assert("p(t) within inequality constraints" && cp[i] <= f->at(i) + 0.0000001);
				if (cp[i] > f->at(i) + 0.0000001 || cpNext[i] > f->at(i) + 0.0000001)
					logPrint("++++++++ SQP: inequality constraint %d: p: %.10lf, p+dp: %.10lf, min val: %.10lf\n", i, cp[i], cpNext[i], f->at(i));

				if (cp[i] < d->at(i) - 0.0000001 || cpNext[i] < d->at(i) - 0.0000001)
					logPrint("++++++++ SQP: inequality constraint %d: p: %.10lf, p+dp: %.10lf, min val: %.10lf\n", i, cp[i], cpNext[i], d->at(i));
			}
		}

		// p must satisfy the bound constraints lo <= p <= hi
		if (minBounds->size() == p.size()){
			for (uint i=0; i<p.size(); i++){
				if (minBounds->at(i) == maxBounds->at(i))
					continue;
//				assert("p(t) within inequality constraints" && cp[i] <= f->at(i) + 0.0000001);
				if (p[i] < minBounds->at(i) - 0.0000001 || pnext[i] < minBounds->at(i) - 0.0000001)
					logPrint("++++++++ SQP: min bound constraint %d: p: %.10lf, p+dp: %.10lf, lo: %.10lf\n", i, p[i], pnext[i], minBounds->at(i));

				if (p[i] > maxBounds->at(i) + 0.0000001 || pnext[i] > maxBounds->at(i) + 0.0000001)
					logPrint("++++++++ SQP: max bound constraint %d: p: %.10lf, p+dp: %.10lf, hi: %.10lf\n", i, p[i], pnext[i], maxBounds->at(i));
			}
		}
	}
}

double SQPFunctionMinimizer::doLineSearch(ObjectiveFunction *function, const dVector &p, const dVector &dp, int maxSteps){
	double alpha = 1.0;

	if (maxSteps <= 1)
		return alpha;

	dVector pc(p);
	double initialValue = function->computeValue(pc);

	for (int j=0; j<maxSteps; j++)
	{
		// p = p + alpha*dp
		add(p, 1, dp, alpha, pc);

		double newLineSearchValue = function->computeValue(pc);

		if (printOutput)
			logPrint("\t--> LINE SEARCH iteration %02d: alpha is %10.10lf, function value is: %10.10lf\n", j, alpha, newLineSearchValue);

		if (newLineSearchValue >= initialValue)
			alpha /= 2; // restore and try again...
		else
			return alpha; // found a better solution!
	}

	return alpha;

	// couldn't find a good value.
	return -1;
}

