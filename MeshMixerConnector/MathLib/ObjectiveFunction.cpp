#include "ObjectiveFunction.h"
#include <Utils/Logger.h>

ObjectiveFunction::ObjectiveFunction(){
}

ObjectiveFunction::~ObjectiveFunction(){
}

//this is a very slow method that evaluates the hessian of the objective function through FD...
void ObjectiveFunction::estimateHessianAt(SparseMatrix& hessian, const dVector& params){
	dVector pSet = params;

	double dp = 10e-6;
	int p = (int)pSet.size();
	resize(hessian, p, p);
	triplets.clear();

	dVector C_P(p), C_M(p), H_i_col(p);

	for (int i=0;i<p;i++){
		double tmpVal = pSet[i];
		pSet[i] = tmpVal + dp;
		C_P = *getGradientAt(pSet);

		pSet[i] = tmpVal - dp;
		C_M = *getGradientAt(pSet);

		//now reset the ith param value
		pSet[i] = tmpVal;
		//and compute the row of the hessian
		H_i_col = (C_P - C_M) / (2 * dp);

		//each vector is a column vector of the hessian, so copy it in place...
		for (int j = i;j < p;j++)
			if (!IS_ZERO(H_i_col[j]))
				triplets.push_back(SMTriplet(j, i, H_i_col[j]));
	}
	hessian.setFromTriplets(triplets.begin(), triplets.end());
}

void ObjectiveFunction::estimateGradientAt(dVector& gradient, const dVector& params){
	dVector pSet = params;

	double dp = 10e-6;
	int p = (int)pSet.size();
	gradient.resize(p);
	double f_P, f_M;
	//this is a very slow method that evaluates the hessian of the objective function through FD...
	for (int i=0;i<p;i++){
		double tmpVal = pSet[i];
		pSet[i] = tmpVal + dp;
		f_P = computeValue(pSet);

		pSet[i] = tmpVal - dp;
		f_M = computeValue(pSet);

		//now reset the ith param value
		pSet[i] = tmpVal;
		gradient[i] = (f_P - f_M)/(2*dp);
	}
	//now restore the parameter set and make sure to tidy up a bit...
	computeValue(pSet);
}

void ObjectiveFunction::testGradientWithFD(const dVector& p){
	dVector FDGradient;
	estimateGradientAt(FDGradient, p);
	dVector* analyticGradient = getGradientAt(p);

	Logger::logPrint("Objective Function: testing gradients...\n");
	for (int i=0;i<p.size();i++){
		double err = FDGradient[i] - analyticGradient->coeff(i);
		if (fabs(err) > 0.0001 && 2*fabs(err) / (fabs(FDGradient[i]) + fabs(analyticGradient->coeff(i))) > 0.001)
			Logger::logPrint("Mismatch element %d: Analytic val: %lf, FD val: %lf. Error: %lf\n", i, analyticGradient->coeff(i), FDGradient[i], err);
	}

}

void ObjectiveFunction::testHessianWithFD(const dVector& p){
	estimateHessianAt(FDHessian, p);
	SparseMatrix* analyticHessian = getHessianAt(p);

	Logger::logPrint("Objective Function: testing hessians...\n");
	for (int i=0;i<p.size();i++){
		for (int j=0;j<p.size();j++){
			double err = FDHessian.coeff(i,j) - analyticHessian->coeff(i,j);
			if (fabs(err) > 0.001)
				Logger::logPrint("Mismatch element %d,%d: Analytic val: %lf, FD val: %lf. Error: %lf\n", i, j, analyticHessian->coeff(i,j), FDHessian.coeff(i,j), err);
		}
	}
}





