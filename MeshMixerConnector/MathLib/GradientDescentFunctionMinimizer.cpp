#include "GradientDescentFunctionMinimizer.h"

GradientDescentFunctionMinimizer::GradientDescentFunctionMinimizer(int p_maxIterations, double p_solveResidual, int p_maxLineSearchIterations, bool p_printOutput){
	maxIterations = p_maxIterations;
	maxLineSearchIterations = p_maxLineSearchIterations;
	printOutput = p_printOutput;
	solveResidual = p_solveResidual;
}

GradientDescentFunctionMinimizer::~GradientDescentFunctionMinimizer(){
}

bool GradientDescentFunctionMinimizer::minimize(ObjectiveFunction *function, dVector &p, double& functionValue){
	if(printOutput){
		Logger::logPrint("Starting Gradient Descent...\n");
	}

	//number of parameters...
	int N = (int) p.size();
	dVector pi(N), dp(N);
	pi = p;

	function->setCurrentBestSolution(pi);

	bool optimizationConverged = false;
	int i;
	for(i=0; i < maxIterations; i++) {
		double currentFunctionValue = function->computeValue(pi);
		dp = *function->getGradientAt(pi);

		if (printOutput)
			Logger::logPrint("Starting iteration %d. Initial function value: %10.10lf. Gradient norm: %lf\n", i, currentFunctionValue, dp.norm());

		double normMag = dp.norm();
//		if (printOutput) Logger::logPrint3("gradient norm: %10.10lf\n", normMag);
		if (normMag < solveResidual){
			optimizationConverged = true;
			break;
		}

		double alpha = doLineSearch(function, pi, dp, maxLineSearchIterations, currentFunctionValue);
		pi += dp * (-alpha);
		function->setCurrentBestSolution(pi);
	}

	functionValue = function->computeValue(pi);

	if(printOutput) {
		Logger::logPrint("Done optimization step. Final function value: %10.10lf\n", functionValue);
		
		if (optimizationConverged){
			Logger::logPrint("Converged in %d iterations!!! Gradient norm: %lf. FunctionValue: %10.10lf\n", i, dp.norm(), functionValue);
		}else{
			Logger::logPrint("Did NOT converge!!! Gradient norm: %lf. FunctionValue: %lf\n", dp.norm(), functionValue);
		}
	}

	//p now holds the parameter values at the start of the iteration...
	p = pi;
	//and done!
	return optimizationConverged;
}



double GradientDescentFunctionMinimizer::doLineSearch(ObjectiveFunction *function, const dVector& pi, const dVector& dp, int maxIterations, double initialFunctionValue){
	// line search now...
	double alpha = 1.0;
	dVector pc(pi);

	for(int j = 0; j < maxIterations; j++) {
		// pi = pi + dp (note, the "-" is because the system we're solving is: grad = -hes * dp
		pc = pi + dp * (-alpha);

		// now check the new function value at this point...
		double newLineSearchValue = function->computeValue(pc);

		if (printOutput)
			Logger::logPrint("\t--> LINE SEARCH iteration %02d: alpha is %10.10lf, function value is: %10.10lf\n", j, alpha, newLineSearchValue);

		if(newLineSearchValue > initialFunctionValue && j < maxIterations-1) {
			// restore and try again...
			alpha /= 2.0;
		} else {
			// found a better solution!
			return alpha;
		}
	}

	// couldn't find a good value. Return what we now have and hope for the best...
	return alpha;
}

